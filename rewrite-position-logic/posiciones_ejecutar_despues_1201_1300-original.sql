CREATE OR REPLACE FUNCTION public.posiciones_ejecutar_despues_1101_1200()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

DECLARE
	var_id_equipo INTEGER;
	var_ult_fecha_hora TIMESTAMP;
	var_fecha_sin_corregir TIMESTAMP;
	var_id_sin_corregir INTEGER;
	var_id_marca_equipo INTEGER;
	-- control de exceso velocidad
	velocidad_maxima_mov INTEGER;
	ult_fec_mov TIMESTAMP;
	ult_vel_mov INTEGER;
	-- control de temperatura
	fec_hor_tem_mov TIMESTAMP;
	ins_tem INTEGER;
	var_sen_tem BOOLEAN;
	-- conteo de paquetes
	var_conteo_paquete INTEGER;
	var_con_paq INTEGER;
	-- correccion odometro teltonika
	var_grupo TEXT;
	var_fec_odo TIMESTAMP;
	var_id_odo INTEGER;
	-- ultima vel>5
	var_id_equipo_extra INTEGER;
	var_fecha_hora_extra TIMESTAMP;
	var_sen_med_comb BOOLEAN;
	var_can_ain1 INTEGER;
	var_can_ain2 INTEGER;
	var_ult_fec_hor_com1 TIMESTAMP;
	var_ult_fec_hor_com2 TIMESTAMP;
	var_velocidad_maxima INTEGER;
	ult_velocidad_eau FLOAT;
	ult_fecha_hora_eau TIMESTAMP;
	ins_exc_vel BOOLEAN;
	-- max_vel_geo
	arr_ids_geo TEXT[];
	arr_max_vel_geo TEXT[];
	ids_geo_var TEXT;
	tot_geo INTEGER;
	ent_geo BOOLEAN;
	ult_fecha_hora_geo TIMESTAMP;
	ult_velocidad_geo FLOAT;
	ins_max_vel_geo BOOLEAN;
    id_geo_max_vel TEXT;
    -- max_vel_pun
	arr_ids_pun TEXT[];
	arr_max_vel_pun TEXT[];
	ids_pun_var TEXT;
	tot_pun INTEGER;
	ent_pun BOOLEAN;
	ult_fecha_hora_pun TIMESTAMP;
	ult_velocidad_pun FLOAT;
	ins_max_vel_pun BOOLEAN;
    id_pun_max_vel TEXT;
    -- max_vel_rut
	arr_ids_rut TEXT[];
	arr_max_vel_rut TEXT[];
	ids_rut_var TEXT;
	tot_rut INTEGER;
	ent_rut BOOLEAN;
	ult_fecha_hora_rut TIMESTAMP;
	ult_velocidad_rut FLOAT;
	ins_max_vel_rut BOOLEAN;
    id_rut_max_vel TEXT;
BEGIN
	var_id_equipo := NULL;
	var_ult_fecha_hora := NULL;
	var_conteo_paquete := NULL;
	var_fecha_sin_corregir := NULL;
	var_id_sin_corregir := NULL;
	var_id_marca_equipo := NULL;
	ult_fec_mov := NULL;
	ins_exc_vel := FALSE;
	var_sen_tem := FALSE;
	var_conteo_paquete := NULL;
	var_con_paq := NULL;
	-- control de temperatura
	ins_tem := 0;
	-- correccion odometro teltonika
	var_grupo := NULL;
	var_fecha_sin_corregir := NULL;
	var_id_sin_corregir := NULL;
	var_fec_odo := NULL;
	var_id_odo := NULL;
	var_sen_med_comb := FALSE;
	var_can_ain1 := 0;
	var_can_ain2 := 0;
	var_ult_fec_hor_com1 := NULL;
	var_ult_fec_hor_com2 := NULL;
	var_velocidad_maxima := 0;
	ult_velocidad_eau := 0;
	ult_fecha_hora_eau := NULL;
    -- max_vel_geo
  	ins_max_vel_geo := FALSE;
	ids_geo_var := NULL;
	tot_geo := 0;
	ent_geo := FALSE;
	ult_fecha_hora_geo := NULL;
	ult_velocidad_geo := 0;
    id_geo_max_vel := NULL;
    -- max_vel_pun
  	ins_max_vel_pun := FALSE;
	ids_pun_var := NULL;
	tot_pun := 0;
	ent_pun := FALSE;
	ult_fecha_hora_pun := NULL;
	ult_velocidad_pun := 0;
    id_pun_max_vel := NULL;
    -- max_vel_rut
  	ins_max_vel_rut := FALSE;
	ids_rut_var := NULL;
	tot_rut := 0;
	ent_rut := FALSE;
	ult_fecha_hora_rut := NULL;
	ult_velocidad_rut := 0;
    id_rut_max_vel := NULL;
	-- consultamos equipo_posicion_ultimo
	BEGIN
		SELECT id_equipo,ult_fecha_hora,conteo_paquete,fecha_sin_corregir,id_sin_corregir
		INTO var_id_equipo,var_ult_fecha_hora,var_conteo_paquete,var_fecha_sin_corregir,var_id_sin_corregir
		FROM equipo_posicion_ultimo
		WHERE id_equipo = NEW.id_equipo;
	EXCEPTION
		WHEN OTHERS THEN
		RAISE EXCEPTION 'Error SELECT equipo_posicion_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
	END;
	-- ######################################################## inicio datos_equipo
	BEGIN
		SELECT me.grupo,me.id_marca_equipo,eq."senTemp",eq.sen_med_comb,mo.velocidad_maxima,mo.fecha_hora_temperatura
		INTO var_grupo,var_id_marca_equipo,var_sen_tem,var_sen_med_comb,var_velocidad_maxima,fec_hor_tem_mov
		FROM
		equipos eq,marca_equipo me,moviles mo
		WHERE
		eq.id_equipo=mo.id_equipo
		AND eq.marca_equipo=me.id_marca_equipo
		AND eq.id_equipo=NEW.id_equipo;
	EXCEPTION
		WHEN OTHERS THEN
		RAISE EXCEPTION 'Error SELECT equipos eq,marca_equipo me,moviles mo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
	END;
	-- ######################################################## fin datos_equipo
	-- ######################################################## inicio desconexion_sensor_combustible
	-- si el equipo tiene sensor(es) de combustible
	IF (var_sen_med_comb = TRUE) THEN
		-- vemos si tiene sensor 1 (ain1)
		BEGIN
			SELECT COUNT(ain1) INTO var_can_ain1 FROM calibracion_sensor_combustible WHERE id_equipo=NEW.id_equipo AND ain1 IS NOT NULL;
			-- si tiene ain1
			IF (var_can_ain1>0) THEN
				-- vemos si este ain1 entrante es < 800
				IF (NEW.ain1 IS NOT NULL AND NEW.ain1 < 800) THEN
					-- vemos si ya se notifico alguna vez
                    BEGIN
						SELECT ult_fecha_hora INTO var_ult_fec_hor_com1 FROM equipo_alerta_ultimo WHERE id_alerta=37 AND id_equipo=NEW.id_equipo;
                    EXCEPTION
						WHEN OTHERS THEN
						RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
					END;
					-- si ya se le inserto alguna vez
					IF (var_ult_fec_hor_com1 IS NOT NULL) THEN
						-- si ya paso mas de una hora de la ultima alerta
						IF (CURRENT_TIMESTAMP - var_ult_fec_hor_com1 > interval '1 hour') THEN
							--se inserta de vuelta la alerta
                            BEGIN
								INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,37,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                          	EXCEPTION
								WHEN OTHERS THEN
								RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
							END;
						END IF;
					ELSE
						-- si nunca se inserto, sera la 1ra desconexion
                        BEGIN
							INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,37,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                  		EXCEPTION
							WHEN OTHERS THEN
							RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
						END;
					END IF;
				END IF;
			END IF;
		EXCEPTION
			WHEN OTHERS THEN
			RAISE EXCEPTION 'Error trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
		END;
		-- vemos si tiene sensor 2 (ain2)
		BEGIN
			SELECT COUNT(ain2) INTO var_can_ain2 FROM calibracion_sensor_combustible WHERE id_equipo=NEW.id_equipo AND ain2 IS NOT NULL;
     	EXCEPTION
			WHEN OTHERS THEN
			RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
		END;
		-- si tiene ain2
		IF (var_can_ain2>0) THEN
			-- vemos si este ain2 entrante es < 800
			IF (NEW.ain2 IS NOT NULL AND NEW.ain2 < 800) THEN
				-- vemos si ya se notifico alguna vez
                BEGIN
					SELECT ult_fecha_hora INTO var_ult_fec_hor_com2 FROM equipo_alerta_ultimo WHERE id_alerta=38 AND id_equipo=NEW.id_equipo;
               	EXCEPTION
					WHEN OTHERS THEN
					RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
				END;
				-- si ya se le inserto alguna vez
				IF (var_ult_fec_hor_com2 IS NOT NULL) THEN						-- si ya paso mas de una hora de la ultima alerta
					IF (CURRENT_TIMESTAMP - var_ult_fec_hor_com2 > interval '1 hour') THEN
						-- se inserta de vuelta la alerta
                        BEGIN
							INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,38,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                       	EXCEPTION
							WHEN OTHERS THEN
							RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
						END;
					END IF;
				ELSE
					-- si nunca se inserto, sera la 1ra desconexion
                     BEGIN
						INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,38,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                     EXCEPTION
						WHEN OTHERS THEN
						RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
					END;
				END IF;
			END IF;
		END IF;
	END IF;
	-- ######################################################## fin desconexion_sensor_combustible
	-- ######################################################## inicio equipo_posicion_ultimo
	-- si nunca se metio posicion
	IF (var_id_equipo IS NULL) THEN 
		-- si la fecha es valida
		IF (NEW.fecha_hora <= '2069-12-31 00:00:00'::TIMESTAMP) THEN
		-- se inserta en equipo_posicion_ultimo
			BEGIN
				INSERT INTO equipo_posicion_ultimo(id_equipo,ult_latitud,ult_longitud,ult_fecha_hora,ult_velocidad,ult_direccion,ult_ip_source,acc,puerto,conteo_paquete,contacto_on,bateria_conectada,ain1,ain2,ain3,ain4,fecha_sin_corregir,id_sin_corregir,temperatura,temperatura2,temperatura3,servidor,protocolo,fecha_hora_transmision,fecha_update,odometro_parcial_metros,odometro_total_metros,gsm_codigo,voltaje_power,cpu_rpm,cpu_odometro,cpu_nivel_combustible,cpu_nro_program,out1,out2,out3,out4,cantidad_satelites,cantidad_senal_gsm) VALUES (NEW.id_equipo,NEW.latitud,NEW.longitud,NEW.fecha_hora,NEW.velocidad,NEW.orientacion,NEW.ip,NEW.acc,NEW.puerto,1,NEW.contacto_on,NEW.bateria_conectada,NEW.ain1,NEW.ain2,NEW.ain3,NEW.ain4,NEW.fecha_hora,NEW.id,NEW.temperatura,NEW.temperatura2,NEW.temperatura3,NEW.servidor,NEW.protocolo,NEW.fecha_hora,CURRENT_TIMESTAMP,NEW.odometro_parcial_metros,NEW.odometro_total_metros,NEW.gsm_codigo,NEW.voltaje_power,NEW.cpu_rpm,NEW.cpu_odometro,NEW.cpu_nivel_combustible,NEW.cpu_nro_program,NEW.out1,NEW.out2,NEW.out3,NEW.out4,NEW.cantidad_satelites,NEW.cantidad_senal_gsm);
			EXCEPTION
				WHEN OTHERS THEN
				RAISE EXCEPTION 'Error INSERT equipo_posicion_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
			END;
		END IF;
	ELSE
		-- vemos si la fecha entrante es el 1ro del dia
		IF ((NEW.fecha_hora::TIMESTAMP > var_ult_fecha_hora::TIMESTAMP) AND (to_char(NEW.fecha_hora::TIMESTAMP, 'DD')<>to_char(var_ult_fecha_hora::TIMESTAMP, 'DD'))) THEN
			var_con_paq = 1;
		ELSE
			var_con_paq = var_con_paq + 1;
		END IF;
		-- inicio para corregir odometro teltonika
		IF (var_grupo='TELTONIKA') THEN
			IF (var_fecha_sin_corregir IS NULL) THEN
				var_fec_odo = NEW.fecha_hora;
				var_id_odo = NEW.id;
			ELSE 
				IF (NEW.fecha_hora::TIMESTAMP < var_fecha_sin_corregir::TIMESTAMP) THEN
					var_fec_odo = NEW.fecha_hora;
					var_id_odo = NEW.id;
				ELSE
					var_fec_odo = var_fecha_sin_corregir;
					var_id_odo = var_id_sin_corregir;
				END IF;
			END IF;
		END IF;
		-- fin para corregir odometro teltonika
		-- actualiza
		IF (NEW.fecha_hora::TIMESTAMP > var_ult_fecha_hora::TIMESTAMP) THEN
			BEGIN
				UPDATE equipo_posicion_ultimo SET ult_latitud=NEW.latitud, ult_longitud=NEW.longitud,ult_fecha_hora=NEW.fecha_hora,ult_velocidad=NEW.velocidad, ult_direccion=NEW.orientacion,ult_ip_source=NEW.ip,acc=NEW.acc,puerto=NEW.puerto,conteo_paquete=var_con_paq,contacto_on=NEW.contacto_on, bateria_conectada=NEW.bateria_conectada,ain1=NEW.ain1,ain2=NEW.ain2,ain3=NEW.ain3,ain4=NEW.ain4,fecha_sin_corregir=var_fec_odo,id_sin_corregir=var_id_odo, temperatura=NEW.temperatura,temperatura2=NEW.temperatura2,temperatura3=NEW.temperatura3,servidor=NEW.servidor,protocolo=NEW.protocolo, fecha_hora_transmision=NEW.fecha_hora::TIMESTAMP,fecha_update=CURRENT_TIMESTAMP,odometro_parcial_metros=NEW.odometro_parcial_metros, odometro_total_metros=NEW.odometro_total_metros,gsm_codigo=NEW.gsm_codigo,voltaje_power=NEW.voltaje_power,cpu_rpm=NEW.cpu_rpm, cpu_odometro=NEW.cpu_odometro,cpu_nivel_combustible=NEW.cpu_nivel_combustible,cpu_nro_program=NEW.cpu_nro_program, out1=NEW.out1,out2=NEW.out2,out3=NEW.out3, out4=NEW.out4, cantidad_satelites=NEW.cantidad_satelites,cantidad_senal_gsm=NEW.cantidad_senal_gsm, din3=NEW.din3,din4=NEW.din4,ble_fuel_frequency_1=NEW.ble_fuel_frequency_1  WHERE id_equipo=var_id_equipo;
			EXCEPTION
				WHEN OTHERS THEN
				RAISE EXCEPTION 'Error UPDATE equipo_posicion_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
			END;
		END IF;	
	END IF;
	-- ######################################################## fin equipo_posicion_ultimo
	-- ######################################################## inicio exceso_de_velocidad
	IF (NEW.velocidad>var_velocidad_maxima) THEN
		-- traemos de equipo_alerta_ultimo la ultima alerta 11 (exceso de velocidad)
        BEGIN
			SELECT ult_velocidad,ult_fecha_hora INTO ult_velocidad_eau,ult_fecha_hora_eau FROM equipo_alerta_ultimo WHERE id_alerta=11 AND id_equipo=NEW.id_equipo;
       	EXCEPTION
				WHEN OTHERS THEN
				RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
		END;
		IF (ult_fecha_hora_eau IS NOT NULL) THEN
			IF(NEW.velocidad>ult_velocidad_eau) THEN
				ins_exc_vel = TRUE;
			END IF;
			IF(NEW.fecha_hora-ult_fecha_hora_eau> INTERVAL '30 minutes') THEN
				ins_exc_vel = TRUE;
			END IF;
		ELSE
			ins_exc_vel = TRUE;
		END IF;
	END IF;
	-- ######################################################## fin exceso_de_velocidad
	-- ######################################################## inicio max_vel_geo
	-- vemos si tiene configuracion 
	BEGIN
		SELECT ids_geo,array_length(regexp_split_to_array(max_vel_geo,'\|'),1),regexp_split_to_array(max_vel_geo,'\|'),regexp_split_to_array(ids_geo,'\|') INTO ids_geo_var,tot_geo,arr_max_vel_geo,arr_ids_geo FROM configuracion_moviles WHERE max_vel_geo ~ '[0-9]' AND id_movil=NEW.id_equipo;
	EXCEPTION
		WHEN OTHERS THEN
		RAISE EXCEPTION 'Error SELECT configuracion_moviles trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
	END;
	-- si configuro max_vel_geo en alguna geo
	IF (ids_geo_var IS NOT NULL) THEN
		-- recorremos c/geo
		FOR i IN 0 .. tot_geo LOOP
			IF (arr_max_vel_geo[i] != '') THEN
				-- si tiene conf vel valida
				IF (arr_max_vel_geo[i]::INTEGER > 0) THEN
					-- vemos si ahora excede la vel max
					IF (NEW.velocidad::INTEGER > arr_max_vel_geo[i]::INTEGER) THEN
						ent_geo := FALSE;
						-- vemos si esta dentro de la geocerca
						BEGIN
							SELECT st_contains(g.geom,st_geomfromtext('point('||NEW.longitud||' '||NEW.latitud||')',4326)) INTO ent_geo FROM geocercas_cliente g WHERE g.id=arr_ids_geo[i]::INTEGER;
						EXCEPTION
							WHEN OTHERS THEN
							RAISE EXCEPTION 'Error SELECT geocercas_cliente trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
						END;
						-- si esta dentro de la geo
						IF (ent_geo = TRUE) THEN
							-- vemos si ya se alerto en esta geo antes
							BEGIN
								SELECT ult_fecha_hora,ult_velocidad INTO ult_fecha_hora_geo,ult_velocidad_geo FROM equipo_alerta_ultimo WHERE id_alerta=102 AND id_equipo=NEW.id_equipo AND id_geocerca=arr_ids_geo[i]::INTEGER;
							EXCEPTION
								WHEN OTHERS THEN
								RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
							END;
							-- si nunca se alerto
							IF(ult_fecha_hora_geo IS NULL) THEN
								-- es la 1ra vez, se inserta
								ins_max_vel_geo := TRUE;
							ELSE
								-- si la (velocidad_actual> velocidad_ultima_alerta)
								IF (NEW.velocidad > ult_velocidad_geo) THEN
									-- insertamos
									ins_max_vel_geo := TRUE;
								END IF;
								-- si hora_del_paquete-fecha_ultima_alerta) >= 30 minutos
								IF (NEW.fecha_hora-ult_fecha_hora_geo > INTERVAL '30 minutes') THEN
									-- insertamos
									ins_max_vel_geo := TRUE;
								END IF;
							END IF;
							IF (ins_max_vel_geo = TRUE) THEN
                            	id_geo_max_vel := arr_ids_geo[i];
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END LOOP;
	END IF;
	-- ######################################################## fin max_vel_geo
   	-- ######################################################## inicio max_vel_pun
	-- vemos si tiene configuracion 
	BEGIN
		SELECT ids_pun,array_length(regexp_split_to_array(max_vel_pun,'\|'),1),regexp_split_to_array(max_vel_pun,'\|'),regexp_split_to_array(ids_pun,'\|') INTO ids_pun_var,tot_pun,arr_max_vel_pun,arr_ids_pun FROM configuracion_moviles WHERE max_vel_pun ~ '[0-9]' AND id_movil=NEW.id_equipo;
	EXCEPTION
		WHEN OTHERS THEN
		RAISE EXCEPTION 'Error SELECT configuracion_moviles trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
	END;
	-- si configuro max_vel_pun en algun punto
	IF (ids_pun_var IS NOT NULL) THEN
		-- recorremos c/pun
		FOR i IN 0 .. tot_pun LOOP
			IF (arr_max_vel_pun[i] != '') THEN
				-- si tiene conf vel valida
				IF (arr_max_vel_pun[i]::INTEGER > 0) THEN
					-- vemos si ahora excede la vel max
					IF (NEW.velocidad::INTEGER > arr_max_vel_pun[i]::INTEGER) THEN
						ent_pun := FALSE;
						-- vemos si esta dentro del punto
						BEGIN
                            SELECT CASE WHEN st_distance(st_transform(st_geomfromtext('point('||NEW.longitud||' '||NEW.latitud||')',4326),32721),st_transform(st_geomfromtext('point('||p.longitud||' '||p.latitud||')',4326),32721))::INTEGER>p.radio then false else true end INTO ent_pun FROM puntos_cliente p WHERE p.id=arr_ids_pun[i]::INTEGER;
                        EXCEPTION
                            WHEN OTHERS THEN
                            RAISE EXCEPTION 'Error SELECT puntos_cliente trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                        END;
						-- si esta dentro del punto
						IF (ent_pun = TRUE) THEN
							-- vemos si ya se alerto en este punto antes
							BEGIN
								SELECT ult_fecha_hora,ult_velocidad INTO ult_fecha_hora_pun,ult_velocidad_pun FROM equipo_alerta_ultimo WHERE id_alerta=109 AND id_equipo=NEW.id_equipo AND id_punto=arr_ids_pun[i]::INTEGER;
							EXCEPTION
								WHEN OTHERS THEN
								RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
							END;
							-- si nunca se alerto
							IF(ult_fecha_hora_pun IS NULL) THEN
								-- es la 1ra vez, se inserta
								ins_max_vel_pun := TRUE;
							ELSE
								-- si la (velocidad_actual> velocidad_ultima_alerta)
								IF (NEW.velocidad > ult_velocidad_pun) THEN
									-- insertamos
									ins_max_vel_pun := TRUE;
								END IF;
								-- si hora_del_paquete-fecha_ultima_alerta) >= 30 minutos
								IF (NEW.fecha_hora-ult_fecha_hora_pun > INTERVAL '30 minutes') THEN
									-- insertamos
									ins_max_vel_pun := TRUE;
								END IF;
							END IF;
							IF (ins_max_vel_pun = TRUE) THEN
                          		id_pun_max_vel := arr_ids_pun[i];
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END LOOP;
	END IF;
	-- ######################################################## fin max_vel_pun
    -- ######################################################## inicio max_vel_rut
	-- vemos si tiene configuracion 
	BEGIN
		SELECT id_ruta,array_length(regexp_split_to_array(max_vel_rut,'\|'),1),regexp_split_to_array(max_vel_rut,'\|'),regexp_split_to_array(id_ruta,'\|') INTO ids_rut_var,tot_rut,arr_max_vel_rut,arr_ids_rut FROM configuracion_moviles WHERE max_vel_rut ~ '[0-9]' AND id_movil=NEW.id_equipo;
	EXCEPTION
		WHEN OTHERS THEN
		RAISE EXCEPTION 'Error SELECT configuracion_moviles trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
	END;
	-- si configuro max_vel_rut en alguna ruta
	IF (ids_rut_var IS NOT NULL) THEN
		-- recorremos c/rut
		FOR i IN 0 .. tot_rut LOOP
			IF (arr_max_vel_rut[i] != '') THEN
				-- si tiene conf vel valida
				IF (arr_max_vel_rut[i]::INTEGER > 0) THEN
					-- vemos si ahora excede la vel max
					IF (NEW.velocidad::INTEGER > arr_max_vel_rut[i]::INTEGER) THEN
						ent_rut := FALSE;
						-- vemos si esta dentro del ruta
						BEGIN
                            SELECT CASE WHEN st_distance(st_transform(st_geomfromtext('point('||NEW.longitud||' '||NEW.latitud||')',4326),32721),st_transform(r.geom,32721))::INTEGER>50 then false else true end INTO ent_rut FROM rutas_cliente r WHERE r.id_ruta=arr_ids_rut[i]::INTEGER;
                        EXCEPTION
                            WHEN OTHERS THEN
                            RAISE EXCEPTION 'Error SELECT rutas_cliente trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                        END;
						-- si esta dentro del rutto
						IF (ent_rut = TRUE) THEN
							-- vemos si ya se alerto en este ruta antes
							BEGIN
								SELECT ult_fecha_hora,ult_velocidad INTO ult_fecha_hora_rut,ult_velocidad_rut FROM equipo_alerta_ultimo WHERE id_alerta=106 AND id_equipo=NEW.id_equipo AND id_ruta=arr_ids_rut[i]::INTEGER;
							EXCEPTION
								WHEN OTHERS THEN
								RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
							END;
							-- si nunca se alerto
							IF(ult_fecha_hora_rut IS NULL) THEN
								-- es la 1ra vez, se inserta
								ins_max_vel_rut := TRUE;
							ELSE
								-- si la (velocidad_actual> velocidad_ultima_alerta)
								IF (NEW.velocidad > ult_velocidad_rut) THEN
									-- insertamos
									ins_max_vel_rut := TRUE;
								END IF;
								-- si hora_del_paquete-fecha_ultima_alerta) >= 30 minutos
								IF (NEW.fecha_hora-ult_fecha_hora_rut > INTERVAL '30 minutes') THEN
									-- insertamos
									ins_max_vel_rut := TRUE;
								END IF;
							END IF;
							IF (ins_max_vel_rut = TRUE) THEN
                          		id_rut_max_vel := arr_ids_rut[i];
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END LOOP;
	END IF;
	-- ######################################################## fin max_vel_rut
   	-- ######################################################## inicio insert_exc_velocidad
    IF (ins_max_vel_geo = TRUE OR ins_max_vel_pun = TRUE OR ins_max_vel_rut = TRUE) THEN
    	IF (ins_max_vel_geo = TRUE) THEN
        	BEGIN
				INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power,id_geocerca) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,102,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power,id_geo_max_vel::INTEGER);
           	EXCEPTION
            	WHEN OTHERS THEN
                RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
           	END;
        END IF;
        IF (ins_max_vel_pun = TRUE) THEN
        	BEGIN
				INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power,id_punto) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,109,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power,id_pun_max_vel::INTEGER);
          	EXCEPTION
				WHEN OTHERS THEN
                RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
          	END;
        END IF;
       	IF (ins_max_vel_rut = TRUE) THEN
        	BEGIN
				INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power,id_ruta) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,106,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power,id_rut_max_vel::INTEGER);
          	EXCEPTION
				WHEN OTHERS THEN
                RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
          	END;
        END IF;
    ELSE
    	IF (ins_exc_vel = TRUE) THEN
        	BEGIN
				INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion, id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,11,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
           	EXCEPTION
				WHEN OTHERS THEN
				RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
			END;
		END IF;
    END IF;
    -- ######################################################## fin insert_exc_velocidad
	-- ######################################################## inicio informe_de_temperatura
	IF (var_id_equipo>0) THEN
		IF (NEW.temperatura IS NOT NULL) THEN
			IF (var_sen_tem = TRUE) THEN
				-- si nunca se inserto la temperatura, sera la 1ra vez
				IF (fec_hor_tem_mov IS NULL) THEN
					-- se inserta la temperatura en tramas_alertas con 28, luego el trigger en alertas filtra
					ins_tem = 1;
				ELSE
					-- si ya paso mas de 5 min de la ultima alerta, se vuelve a insertar
					IF ((NEW.fecha_hora - fec_hor_tem_mov) > interval '5 minutes') THEN
						ins_tem = 1;
					END IF;
				END IF;
				-- si hay que insertar INFORME DE TEMPERATURA
				IF (ins_tem = 1) THEN
					BEGIN
						INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion, id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,temperatura,temperatura2,temperatura3,gsm_codigo,voltaje_power,contacto_on,bateria_conectada) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,28,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.temperatura,NEW.temperatura2,NEW.temperatura3,NEW.gsm_codigo,NEW.voltaje_power,NEW.contacto_on,NEW.bateria_conectada);
					EXCEPTION
						WHEN OTHERS THEN
						RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
					END;
					-- actualizamos en moviles, fecha_hora_temperatura
					BEGIN
						UPDATE moviles
						SET fecha_hora_temperatura = NEW.fecha_hora
						WHERE id_equipo	= var_id_equipo;
					EXCEPTION
						WHEN OTHERS THEN
						RAISE EXCEPTION 'Error UPDATE moviles trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
					END;
				END IF;
			END IF;
		END IF;
	END IF;
	-- ######################################################## fin informe_de_temperatura
	-- ######################################################## inicio ultima_velocidad
	var_id_equipo_extra := NULL;
	var_fecha_hora_extra := NULL;
	-- vemos si esta velocidad es >5
	IF (NEW.velocidad>5) THEN
		-- si ya hubo algun paq previo con vel>5
		BEGIN
			SELECT id_equipo,fecha_hora
			INTO var_id_equipo_extra,var_fecha_hora_extra
			FROM equipo_posicion_ultimo_extra
			WHERE 
			id_equipo = NEW.id_equipo;
		EXCEPTION
			WHEN OTHERS THEN
			RAISE EXCEPTION 'Error SELECT equipo_posicion_ultimo_extra trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
		END;
		-- si ya hubo
		IF (var_id_equipo_extra IS NOT NULL) THEN
			-- vemos si la fecha entrante es superior a la previa y menor a la actual
			IF (NEW.fecha_hora>var_fecha_hora_extra AND NEW.fecha_hora::DATE=CURRENT_DATE) THEN
				-- se actualiza
				BEGIN
					UPDATE equipo_posicion_ultimo_extra
					SET
					fecha_hora=NEW.fecha_hora,
					velocidad=NEW.velocidad,
					latitud=NEW.latitud,
					longitud=NEW.longitud,
					orientacion=NEW.orientacion,
					ip=NEW.ip,
					puerto=NEW.puerto,
					protocolo=NEW.protocolo,
					servidor=NEW.servidor,
					contacto_on=NEW.contacto_on,
					bateria_conectada=NEW.bateria_conectada,
					temperatura=NEW.temperatura
					WHERE 
					id_equipo=NEW.id_equipo;
				EXCEPTION
					WHEN OTHERS THEN
					RAISE EXCEPTION 'Error UPDATE equipo_posicion_ultimo_extra trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
				END;
			END IF;
		ELSE
			-- sino, sera la 1ra vez, se inserta
			BEGIN
				INSERT INTO equipo_posicion_ultimo_extra (id_equipo,fecha_hora,velocidad,latitud,longitud,orientacion,ip,puerto,protocolo,servidor,contacto_on,bateria_conectada,temperatura) VALUES (NEW.id_equipo,NEW.fecha_hora,NEW.velocidad,NEW.latitud,NEW.longitud,NEW.orientacion,NEW.ip,NEW.puerto,NEW.protocolo,NEW.servidor,NEW.contacto_on,NEW.bateria_conectada,NEW.temperatura);
			EXCEPTION
				WHEN OTHERS THEN
				RAISE EXCEPTION 'Error INSERT equipo_posicion_ultimo_extra trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
			END;
			
		END IF;
	END IF;
	-- ######################################################## fin ultima_velocidad
	RETURN NEW;
END;

$function$
;

COMMENT ON FUNCTION public.posiciones_ejecutar_despues_1101_1200() IS 'realiza varios controles luego de insertar el paquete de posicion';

