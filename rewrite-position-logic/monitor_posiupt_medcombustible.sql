CREATE OR REPLACE FUNCTION public.monitor_posiupt_medcombustible(NEW tramas_posiciones_0_100, OUT result integer )
 RETURNS integer  AS
$func$
DECLARE
    var_ult_fec_hor_com1 TIMESTAMP;
    var_ult_fec_hor_com2 TIMESTAMP;
    var_can_ain1 INTEGER;
	var_can_ain2 INTEGER;
BEGIN
    var_ult_fec_hor_com1 := NULL;
	var_ult_fec_hor_com2 := NULL;
    var_can_ain1 := 0;
	var_can_ain2 := 0;
    
    -- vemos si tiene sensor 1 (ain1)
    BEGIN
        SELECT COUNT(ain1) INTO var_can_ain1 FROM calibracion_sensor_combustible WHERE id_equipo=NEW.id_equipo AND ain1 IS NOT NULL;
        -- si tiene ain1
        IF (var_can_ain1>0) THEN
            -- vemos si este ain1 entrante es < 800
            IF (NEW.ain1 IS NOT NULL AND NEW.ain1 < 800) THEN
                -- vemos si ya se notifico alguna vez
                BEGIN
                    SELECT ult_fecha_hora INTO var_ult_fec_hor_com1 FROM equipo_alerta_ultimo WHERE id_alerta=37 AND id_equipo=NEW.id_equipo;
                EXCEPTION
                    WHEN OTHERS THEN
                    RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                END;
                -- si ya se le inserto alguna vez
                IF (var_ult_fec_hor_com1 IS NOT NULL) THEN
                    -- si ya paso mas de una hora de la ultima alerta
                    IF (CURRENT_TIMESTAMP - var_ult_fec_hor_com1 > interval '1 hour') THEN
                        --se inserta de vuelta la alerta
                        BEGIN
                            INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,37,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                        EXCEPTION
                            WHEN OTHERS THEN
                            RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                        END;
                    END IF;
                ELSE
                    -- si nunca se inserto, sera la 1ra desconexion
                    BEGIN
                        INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,37,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                    EXCEPTION
                        WHEN OTHERS THEN
                        RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                    END;
                END IF;
            END IF;
        END IF;
    EXCEPTION
        WHEN OTHERS THEN
        RAISE EXCEPTION 'Error trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
    END;
    
        -- vemos si tiene sensor 2 (ain2)
    BEGIN
        SELECT COUNT(ain2) INTO var_can_ain2 FROM calibracion_sensor_combustible WHERE id_equipo=NEW.id_equipo AND ain2 IS NOT NULL;
    EXCEPTION
        WHEN OTHERS THEN
        RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
    END;

    -- si tiene ain2
    IF (var_can_ain2>0) THEN
        -- vemos si este ain2 entrante es < 800
        IF (NEW.ain2 IS NOT NULL AND NEW.ain2 < 800) THEN
            -- vemos si ya se notifico alguna vez
            BEGIN
                SELECT ult_fecha_hora INTO var_ult_fec_hor_com2 FROM equipo_alerta_ultimo WHERE id_alerta=38 AND id_equipo=NEW.id_equipo;
            EXCEPTION
                WHEN OTHERS THEN
                RAISE EXCEPTION 'Error SELECT equipo_alerta_ultimo trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
            END;
            -- si ya se le inserto alguna vez
            IF (var_ult_fec_hor_com2 IS NOT NULL) THEN						-- si ya paso mas de una hora de la ultima alerta
                IF (CURRENT_TIMESTAMP - var_ult_fec_hor_com2 > interval '1 hour') THEN
                    -- se inserta de vuelta la alerta
                    BEGIN
                        INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,38,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                    EXCEPTION
                        WHEN OTHERS THEN
                        RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                    END;
                END IF;
            ELSE
                -- si nunca se inserto, sera la 1ra desconexion
                    BEGIN
                    INSERT INTO tramas_alertas (mensaje,latitud,longitud,velocidad,orientacion,id_equipo,ip,observacion,id_alerta,fecha_equipo,fecha_hora,cantidad_satelites,auxiliar_txt,fecha_insert,puerto,cantidad_senal_gsm,senal_gprs,gsm_codigo,voltaje_power) values (NEW.mensaje,NEW.latitud,NEW.longitud,NEW.velocidad,NEW.orientacion,NEW.id_equipo,NEW.ip,NULL,38,NEW.fecha_equipo,NEW.fecha_hora,NEW.cantidad_satelites,NULL,now(),NEW.puerto,NEW.cantidad_senal_gsm,NEW.senal_gprs,NEW.gsm_codigo,NEW.voltaje_power);
                    EXCEPTION
                    WHEN OTHERS THEN
                    RAISE EXCEPTION 'Error INSERT tramas_alertas trigger posiciones_ejecutar_despues_1101_1200: (%)', SQLERRM;
                END;
            END IF;
        END IF;
    END IF;


END;

$func$  LANGUAGE plpgsql;


-- COMMENT ON FUNCTION public.monitor_posiupt_medcombustible() IS 'realiza el control de combustible';
