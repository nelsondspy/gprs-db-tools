CREATE OR REPLACE FUNCTION f_trama_reemplazo(_tabla in text, OUT result integer ) RETURNS integer  AS
$func$
DECLARE
   trigger_nombre text;
   trigger_def text;
   tabla_old text; 
  
BEGIN
    tabla_old := _tabla || '_old';
BEGIN 

    EXECUTE format('SELECT * FROM  %s WHERE 1 = 0', _tabla) ; 
    
    EXECUTE format('SELECT tgname FROM  pg_trigger WHERE tgrelid = %L::regclass', _tabla) 
        INTO trigger_nombre;
    
    EXECUTE format('SELECT action_statement as definition FROM information_schema.triggers WHERE event_object_table = %L', _tabla) 
        INTO trigger_def;

        raise notice 'trigger_def: %', trigger_def;

END;

    EXECUTE format('ALTER TABLE %s DISABLE TRIGGER %s', _tabla , trigger_nombre) ;

    EXECUTE format('ALTER TRIGGER %s ON %s RENAME TO %s', trigger_nombre, _tabla, trigger_nombre || '_old' );

    --renombra tabla !!
    EXECUTE format('ALTER TABLE %s RENAME TO %s', _tabla,  tabla_old);


    EXECUTE format('CREATE TABLE %s (LIKE %s INCLUDING ALL)', _tabla, tabla_old) ;

    -- EXECUTE PROCEDURE posiciones_ejecutar_despues_2201_2300()
    EXECUTE format('CREATE TRIGGER %s AFTER INSERT ON %s FOR EACH ROW %s', trigger_nombre, _tabla, trigger_def );
    result := 0 ;
END

$func$  LANGUAGE plpgsql;
