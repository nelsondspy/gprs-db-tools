CREATE OR REPLACE FUNCTION f_trama_volcado(_tabla_old in text, _tabla in text, fecha_desde in date  , OUT result integer ) RETURNS integer  AS
$func$
DECLARE
 
rec RECORD; 
dia_open timestamp ; 
dia_close timestamp ;  
 
BEGIN
	
	dia_open := now()::timestamp;
	dia_close := now()::timestamp;

	-- deshabilitar los triggers solo en esta sesion para evitar actualizar otras tablas
	RAISE NOTICE  'punto 1 ';
	
	SET session_replication_role = replica;
	FOR rec IN 
		SELECT dd FROM generate_series 
			(fecha_desde::timestamp  - interval '1 day' , now()::timestamp, '1 day'::interval) dd 
			ORDER BY dd DESC  
	LOOP 
	   
			EXECUTE format('INSERT INTO %s SELECT * FROM %s WHERE fecha_hora >=%L::timestamp AND fecha_hora <= %L::timestamp '
				, _tabla , _tabla_old, dia_open, dia_close);
			EXECUTE format('DELETE FROM %s WHERE fecha_hora >=%L::timestamp  AND fecha_hora <= %L::timestamp ', _tabla_old, dia_open, dia_close ); 
			dia_close := dia_open;
			dia_open := rec.dd;

		
    END LOOP;

-- config por defecto nuevamente 
SET session_replication_role = DEFAULT;
      
END

$func$  LANGUAGE plpgsql;

