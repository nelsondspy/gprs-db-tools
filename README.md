1. Renombrar la tabla recibida como parámetro a  "*nombre_tabla_old*"
y generar una nueva tabla sin datos con el nombre original "*nombre_tabla*" con las restricciones y el trigger asociado.
Por ejemplo:


```
select f_trama_reemplazo('tramas_posiciones_1301_1400');
```


2. Realizar el volcado de datos desde "*nombre_tabla_old*" a "*nombre_tabla*" que inicia  desde la fecha actual hasta la fecha recibida como parámetro, es decir de forma descendente.
Observaciones:
- Los registros volcados son borrados de "*nombre_tabla_old*"
- Para la sesión en la que se ejecuta esta función de desactivan todos los triggers para evitar modificar *equipo_posicion_ultimo* y otras tablas normalmente actualizadas por el trigger.

Por ejemplo:
 
```
select f_trama_volcado('tramas_posiciones_1301_1400_old', 
		        'tramas_posiciones_1301_1400', 
		        '2019-12-18'::date); 
```


